<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Lead;
use App\Mail\Leads;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;


class LeadsActionController extends Controller
{
    public function leads(){
        try {
            $lead = Lead::create(request()->except('_token'));
            // dd($lead->nome, $lead->email, $lead->telefone);

            if($lead){
                Mail::to(env('EMAIL_DESTINATARIO'))->send(new Leads(
                    $lead->nome,
                    $lead->email,
                    $lead->telefone
                ));
            }
        } catch (\Throwable $th) {
            // Log::error($th);
            return redirect()->back()->with('erro', "Erro no cadastro de informações!");
        }
        return redirect()->back()->with('sucesso', 'Enviado com sucesso!');
    }
}

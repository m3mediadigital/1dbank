@component('mail::message')
# Nome

<ul style="list-style: none">
        <li>{{ $nome }}</li>
</ul>

# E-mail
<ul style="list-style: none">
    <li>{{ $email }}</li>
</ul>

# Telefone
<ul style="list-style: none">
    <li>{{ $telefone }}</li>
</ul>

<br>
{{ config('app.name') }}
@endcomponent